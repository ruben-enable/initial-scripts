#!/bin/sh

apt-get install zfsutils-linux -y

ilan=$(ifconfig | grep vlan.ilan)
if [[ $ilan == "" ]]; then #first time
    #add internal (vRack) interface
	sudo apt-get -y install vlan
	sudo modprobe 8021q
	sudo echo "8021q" >> /etc/modules
cat > /etc/netplan/01-netcfg.yaml << EOF
network:
        version: 2
        renderer: networkd
        ethernets:
                eno2: {}

        vlans:
                vlan.ilan:
                        id: 2525
                        link: eno2
                        addresses: [10.1.0.2/12]
EOF
	sudo netplan apply

#init Lxd	
cat <<EOF | sudo lxd init
no
yes
default
zfs
yes
no
100GB
no
yes
lxdbr0
auto
auto
no
yes
no
EOF
fi

exit 0