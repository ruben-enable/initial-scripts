#!/bin/sh

apt-get install zfsutils-linux -y

ilan=$(ifconfig | grep vlan.ilan)
if [[ $ilan == "" ]]; then #first time

#add additional (failover) ip's
cat > /etc/netplan/50-cloud-init.yaml << EOF
network:
    version: 2
    ethernets:
        eno1:
            dhcp4: true
            match:
                macaddress: a4:bf:01:3f:e4:90
            set-name: eno1
            addresses:
            - 51.89.26.56/29
EOF

    #add internal (vRack) interface
	sudo apt-get -y install vlan
	sudo modprobe 8021q
	sudo echo "8021q" >> /etc/modules
cat > /etc/netplan/01-netcfg.yaml << EOF
network:
        version: 2
        renderer: networkd
        ethernets:
                eno2: {}

        vlans:
                vlan.ilan:
                        id: 2525
                        link: eno2
                        addresses: [10.1.0.1/12]
EOF
	sudo netplan apply

#init Lxd	
cat <<EOF | sudo lxd init
no
yes
default
zfs
yes
no
100GB
no
yes
lxdbr0
auto
auto
no
yes
no
EOF
fi

exit 0